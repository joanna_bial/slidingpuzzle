#ifndef SCORESPAGE_H
#define SCORESPAGE_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QStackedLayout>
#include <QTableView>
#include <QStandardItemModel>
#include <QHeaderView>
#include <QScrollBar>

#include "MainWindow.h"
#include "Score.h"

/**
 * \brief Klasa wyświetlająca wynikami zakończonych gier
 *
 * Obsługuje zdarzenia występujące w oknie wyświetlania wyników zakończonych gier
 *
 */
class ScoresPage : public QWidget
{
    Q_OBJECT

public:
    explicit ScoresPage(QWidget *parent, QStackedLayout *const stackedLayout);

public slots:
    void goToMenu();
    void loadPage();

private:
    QStackedLayout *const _stackedLayout;
    QPushButton *const _goToMenuButton;
    QTableView * _table;
    QStandardItemModel * _model;

    std::vector<Score> _scores;

    void ReadScores();
};

#endif // SCORESPAGE_H
