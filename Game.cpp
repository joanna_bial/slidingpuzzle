#include <math.h>

#include "Game.h"

/**
 * \brief Konstruktor paramatryczny
 *
 * Tworzy obiekt klasy Game
 * \param parameters parametry gry
 * \param currentBoardState aktualny stan planszy
 * \param time czas, który upłynął od rozpoczęcia gry
 * \param moves liczba wykonanych ruchów od rozpoczęcia gry
 * \param playerName nazwa gracza
 *
 */
Game::Game(GameParameters parameters, std::vector<int> currentBoardState, int time, int moves, std::string playerName) :
    _parameters(parameters),
    _currentBoardState(currentBoardState),
    _playerName(playerName),
    _time(time),
    _moves(moves)
{
}

/**
 * \brief Konstruktor paramatryczny
 *
 * Tworzy obiekt klasy Game
 * \param parameters parametry gry
 * \param playerName nazwa gracza
 *
 *
 */
Game::Game(GameParameters parameters, std::string playerName) :
    _parameters(parameters),
    _playerName(playerName),
    _time(0),
    _moves(0)
{
}

/**
 * \brief Metoda zwracająca pozycję początkową pustej komórki
 *
 * \return pozycja początkowa pustej komórki
 *
 */
Position Game::GetInitialBlankFieldPosition() const
{
    if (_parameters.InitialBoardState.size() != 0)
    {
        int index = 0;

        for (auto val : _parameters.InitialBoardState)
        {
            if (val == 0)
                break;

            index++;
        }

        return Position(index%_parameters.Dimension, index/_parameters.Dimension);
    }
    else
    {
        return Position(_parameters.Dimension-1, _parameters.Dimension-1);
    }
}

/**
* \brief Metoda zwracająca pozycję aktualną pustej komórki
*
* \return pozycja aktualna pustej komórki
*
*/
Position Game::GetCurrentBlankFieldPosition() const
{
    if (_currentBoardState.size() != 0)
    {
        int index = 0;

        for (auto val : _currentBoardState)
        {
            if (val == 0)
                break;

            index++;
        }

        return Position(index%_parameters.Dimension, index/_parameters.Dimension);
    }
    else
    {
        return Position(_parameters.Dimension-1, _parameters.Dimension-1);
    }
}

/**
* \brief Metoda aktualizująca stan gry
*
* Generuje tekst, który może zostać użyty w celu późniejszego wczytania gry
* \param currentBoardState aktualny stan gry
* \param time czas, który upłynął
* \param moves liczba wykonanych ruchów
*
*/
void Game::Update(std::vector<int> currentBoardState, int time, int moves)
{
    _currentBoardState = currentBoardState;
    _time = time;
    _moves = moves;
}

/**
* \brief Metoda zwracająca string opisujący grę
*
*  Generuje tekst, który może zostać użyty w celu późniejszego wczytania gry
* \return string
*
*/
std::string Game::GetString() const
{
    if (_currentBoardState.size() > 0)
    {
        std::string content;

        content+= "InitialBoardState:";
        for (auto value : _parameters.InitialBoardState)
        {
            content += std::to_string(value);
            content += ",";
        }
        content += "\n";

        content+= "CurrentBoardState:";
        for (auto value : _currentBoardState)
        {
            content += std::to_string(value);
            content += ",";
        }
        content += "\n";

        content+= "Time:";
        content += std::to_string(_time);
        content += "\n";

        content+= "Moves:";
        content += std::to_string(_moves);
        content += "\n";

        content+= "PlayerName:";
        content += _playerName;
        content += "\n";

        content+= "GameName:";
        content += _parameters.GameName;

        return content;
    }
    else
    {
        return "";
    }
}

/**
* \brief Metoda zwracająca czas, który upłynął od rozpoczęcia gry
*
* \return czas, który upłynął od rozpoczęcia gry
*
*/
int Game::GetElapsedTime() const
{
    return _time;
}

/**
* \brief Metoda zwracająca nazwę gracza
*
* \return nazwa gracza
*
*/
std::string Game::GetPlayerName() const
{
    return _playerName;
}

/**
* \brief Metoda zwracająca stan początkowy planszy
*
* \return initialBoardState wektor wartości w początkowej kolejności
*
*/
std::vector<int> Game::GetInitialBoardState() const
{
    return _parameters.InitialBoardState;
}

/**
* \brief Metoda zwracająca stan aktualny planszy
*
* \return wektor wartości w aktualnej kolejności
*
*/
std::vector<int> Game::GetCurentBoardState() const
{
    return _currentBoardState;
}

/**
* \brief Metoda zwracająca liczbę ruchów wykonanych od rozpoczęcia gry
*
* \return liczba ruchów wykonanych od rozpoczęcia gry
*
*/
int Game::GetMoves() const
{
    return _moves;
}

/**
* \brief Metoda zwracająca parametry gry
*
* \return parametry gry
*
*/
GameParameters Game::GetParameters() const
{
    return _parameters;
}
