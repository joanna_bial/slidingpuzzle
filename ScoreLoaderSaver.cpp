#include "ScoreLoaderSaver.h"

/**
 * \brief Konstruktor
 *
 * Tworzy obiekt klasy ScoreLoaderSaver
 * Ustawia nazwę pliku na "scores.txt"
 *
 */
ScoreLoaderSaver::ScoreLoaderSaver()
    : LoaderSaver("scores.txt")
{
}

/**
 * \brief Metoda zapisująca wynik
 *
 * \param score wynik, który ma zostać zapisany
 * \return informacja o powodzeniu operacji
 *
 */
bool ScoreLoaderSaver::Save(Score score)
{
    auto scoreString = Serialize(score);
    return WriteFile(scoreString);
}

/**
 * \brief  Metoda wczytująca wynik
 *
 * \return wczytany wynik
 *
 */
std::vector<Score> ScoreLoaderSaver::Load()
{
    auto lines = ReadFile();
    return Deserialize(lines);
}

/**
 * \brief  Metoda serializująca wynik
 *
 * \param score wynik, który ma zostać zapisany
 * \return string
 *
 */
std::string ScoreLoaderSaver::Serialize(Score score)
{
    return score.GetString();
}

/**
 * \brief Metoda deserializująca wynik
 *
 * \param lines linie odczytane z pliku
 * \return game
 *
 */
std::vector<Score> ScoreLoaderSaver::Deserialize(std::vector<std::string> lines)
{
    std::vector<Score> scores;

    int dimension;
    int time;
    int moves;
    std::string playerName;
    std::string gameName;

    for (auto line : lines)
    {
        std::string flag = "";
        auto it = line.begin();
        while (*it != ':')
        {
            flag += *it;
            it++;
        }

        it++;
        if (flag == "PlayerName")
        {
            playerName = GetStringFromLine(it, line);
        }
        else if (flag == "GameName")
        {
            gameName = GetStringFromLine(it, line);
        }
        else if (flag == "Dimension")
        {
            dimension = GetIntFromLine(it, line);
        }
        else if (flag == "Time")
        {
            time = GetIntFromLine(it, line);
        }
        else if (flag == "Moves")
        {
            moves = GetIntFromLine(it, line);
        }
        else if (flag == "end")
        {
            scores.push_back(Score(playerName, gameName, moves, time, dimension));
        }
    }

    return scores;
}

bool ScoreLoaderSaver::ShouldClear() const
{
    return false;
}
