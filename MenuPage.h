#ifndef MENUPAGE_H
#define MENUPAGE_H

#include <QWidget>
#include <QVBoxLayout>
#include <QStackedLayout>
#include <QPushButton>

#include "MainWindow.h"
#include "GamePage.h"
#include "Game.h"
#include "GameLoaderSaver.h"

/**
 * \brief Klasa wyświetlająca stronę z menu
 *
 * Obsługuje zdarzenia występujące w oknie menu
 *
 */
class MenuPage : public QWidget
{
    Q_OBJECT

public:
    explicit MenuPage(QWidget *parent, QStackedLayout *const stackedLayout);
    QPushButton* AddButton(QString buttonLabel, int indexButton);

signals:
    void goToScoresButtonPressed();
    void gotToLoadGameButtonPressed();
    void goToNewGameButtonPressed();

public slots:
    void goToGame();
    void goToScores();
    void goToLoadGame();
    void goToNewGame();
    void enableResumeButton();
    void disableResumeButton();

private:
    QStackedLayout *const _stackedLayout;
    QVBoxLayout *const _boxLayout;
    QPushButton * _resumeButton;
};

#endif // MENUPAGE_H
