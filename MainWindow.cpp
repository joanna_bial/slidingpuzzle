#include "MainWindow.h"
#include "MenuPage.h"
#include "NewGamePage.h"
#include "LoadGamePage.h"
#include "GamePage.h"
#include "ScoresPage.h"
#include "ResultsPage.h"
#include "QDesktopWidget"

/**
 * \brief Konstruktor parametryczny
 *
 * Tworzy obiekt klasy MainWindow
 * \param parent widget wywołujący konstruktor
 *
 */
MainWindow::MainWindow(QWidget *parent)
  : QWidget(parent),
    _stackedLayout(new QStackedLayout(this))
{
    setWindowTitle(tr("Slide a puzzle"));
    setFixedSize(1100, 800);

    NewGamePage  *const newGamePage = new NewGamePage(this, _stackedLayout);
    LoadGamePage  *const loadGamePage = new LoadGamePage(this, _stackedLayout);
    GamePage  *const gamePage = new GamePage(this, _stackedLayout);
    ScoresPage  *const scoresPage = new ScoresPage(this, _stackedLayout);
    ResultsPage  *const resultsPage = new ResultsPage(this, _stackedLayout); 
    MenuPage *const menuPage = new MenuPage(this, _stackedLayout);

    _stackedLayout->insertWidget(NEW_GAME_INDEX, newGamePage);
    _stackedLayout->insertWidget(LOAD_GAME_INDEX, loadGamePage);
    _stackedLayout->insertWidget(GAME_INDEX, gamePage);
    _stackedLayout->insertWidget(SCORES_INDEX, scoresPage);
    _stackedLayout->insertWidget(RESULTS_INDEX, resultsPage);
    _stackedLayout->insertWidget(MENU_INDEX, menuPage);

    _stackedLayout->setCurrentIndex(MENU_INDEX);

    connect(resultsPage, &ResultsPage::gameFinished, menuPage, &MenuPage::disableResumeButton);
    connect(gamePage, &GamePage::gameNotFinished, menuPage, &MenuPage::enableResumeButton);
    connect(gamePage, SIGNAL(gameFinished(Score)), resultsPage, SLOT(showResults(Score)));
    connect(menuPage, &MenuPage::goToScoresButtonPressed, scoresPage, &ScoresPage::loadPage);
    connect(menuPage, &MenuPage::gotToLoadGameButtonPressed, loadGamePage, &LoadGamePage::loadPage);
    connect(menuPage, &MenuPage::goToNewGameButtonPressed, newGamePage, &NewGamePage::loadGames);
}
