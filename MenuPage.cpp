#include <QPushButton>

#include "MenuPage.h"
#include "NewGamePage.h"
#include "LoadGamePage.h"
#include "GamePage.h"
#include "MainWindow.h"
#include "ScoresPage.h"
#include "GameLoaderSaver.h"

/**
 * \brief Konstruktor parametryczny
 *
 * Tworzy obiekt klasy MenuPage
 * \param parent widget wywołujący konstruktor
 * \param stackedLayout layout przechowujący informacje o pozostałych widgetach
 *
 */
MenuPage::MenuPage(QWidget *parent, QStackedLayout *const stackedLayout)
  : QWidget(parent),
    _stackedLayout(stackedLayout),
    _boxLayout(new QVBoxLayout(this))
{
    _boxLayout->setContentsMargins(400, 0, 400, 0);

    AddButton("NEW GAME", NEW_GAME_INDEX);
    AddButton("LOAD GAME", LOAD_GAME_INDEX);
    _resumeButton = AddButton("RESUME", GAME_INDEX);
    AddButton("SCORES", SCORES_INDEX);

    GameLoaderSaver gameLoaderSaver;
    _resumeButton->setEnabled(static_cast<bool>(gameLoaderSaver.Load()));
}

/**
 * \brief  Metoda dodająca przycisk do okna
 *
 * \param buttonLabel napis na przycisku
 * \param indexButton id przycisku
 *
 */
QPushButton* MenuPage::AddButton(QString buttonLabel, int indexButton)
{
    QString label = QString(buttonLabel);
    QPushButton *button = new QPushButton(label, this);
     button->setMinimumHeight(60);
     button->setFont(QFont("Times", 20, QFont::Bold));
    _boxLayout->addWidget(button);

    switch (indexButton)
    {
    case GAME_INDEX:
        connect(button, &QPushButton::clicked, this, &MenuPage::goToGame);
        break;

    case SCORES_INDEX:
        connect(button, &QPushButton::clicked, this, &MenuPage::goToScores);
        break;

    case LOAD_GAME_INDEX:
        connect(button, &QPushButton::clicked, this, &MenuPage::goToLoadGame);
        break;

    case NEW_GAME_INDEX:
        connect(button, &QPushButton::clicked, this, &MenuPage::goToNewGame);
        break;

    default:
        break;
    }

    return button;
}

/**
 * \brief Slot przekierowujący do kontynuowania rozpoczętej gry
 *
 */
void MenuPage::goToGame()
{
    GameLoaderSaver gameLoaderSaver;
    auto game = gameLoaderSaver.Load();

    auto* gameWidget = (GamePage*)(_stackedLayout->widget(GAME_INDEX));
    gameWidget->ContinueGame(std::move(game));

    _stackedLayout->setCurrentIndex(GAME_INDEX);
}

/**
 * \brief Slot przekierowujący do okna z wynikami rozegranych gier
 *
 */
void MenuPage::goToScores()
{
    emit goToScoresButtonPressed();
    _stackedLayout->setCurrentIndex(SCORES_INDEX);
}

/**
 * \brief Slot przekierowujący do okna z wczytaniem jednej z rozpoczętych gier
 *
 */
void MenuPage::goToLoadGame()
{
    emit gotToLoadGameButtonPressed();
    _stackedLayout->setCurrentIndex(LOAD_GAME_INDEX);
}

/**
 * \brief Slot przekierowujący do okna tworzenia nowej gry
 *
 */
void MenuPage::goToNewGame()
{
    emit goToNewGameButtonPressed();
    _stackedLayout->setCurrentIndex(NEW_GAME_INDEX);
}

/**
 * \brief Slot włączający przycisk kontynuowania gry
 *
 */
void MenuPage::enableResumeButton()
{
    _resumeButton->setEnabled(true);
}

/**
 * \brief Slot wyłączający przycisk kontynuowania gry
 *
 */
void MenuPage::disableResumeButton()
{
    _resumeButton->setDisabled(true);
}
