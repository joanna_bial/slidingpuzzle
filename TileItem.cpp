#include <QGraphicsTextItem>

#include "TileItem.h"

int TileItem::PuzzleSideLength;
int TileItem::Dimension;

/**
 * \brief Konstruktor parametryczny
 *
 * Tworzy obiekt klasy TileItem
 * \param value wartość
 * \param scena scena, w której znajdzie się kwadrat
 * \param position pozycja kwadratu na planszy
 *
 */
TileItem::TileItem(int value, QGraphicsScene *scene, Position position) :
    _textItem(new QGraphicsTextItem(QString::number(value))),
    _scene(scene),
    _value(value)
{
    QRectF rect;
    rect.setSize(QSize(PuzzleSideLength,PuzzleSideLength));
    _rectItem = _scene->addRect(rect, QPen(Qt::NoBrush), QBrush(QColor("gray")));

    _textItem->setFont(QFont("Times", 55-Dimension*4, QFont::Bold));
    _textItem->setZValue(1);
    _scene->addItem(_textItem);

    MoveToPosition(position);
}

/**
 * \brief Metoda sprawdzająca czy kliknięcie myszki odbyło się w obszarze kwadratu
 *
 * \param mousePoint pozycja, która została kliknięta
 * \return true, jeżeli pozycja należy do kwadratu
 *
 */
bool TileItem::ContainsMousePosition(QPointF mousePoint)
{
    return (mousePoint.rx() >= _rectItem->pos().rx() && mousePoint.rx()< _rectItem->pos().rx()+PuzzleSideLength
            && mousePoint.ry() >= _rectItem->pos().ry() && mousePoint.ry()< _rectItem->pos().ry()+PuzzleSideLength);
}

/**
 * \brief Metoda przesuwająca kwadrat do innej pozycji
 *
 * \param position pozycja, do której ma zostać przesunięty kwadrat
 *
 */
void TileItem::MoveToPosition(Position position)
{
    _position = position;

    _rectItem->setPos(QPointF(_position.GetX()*PuzzleSideLength, _position.GetY()*PuzzleSideLength));

    if (position.GetX()+1 + _position.GetY()*Dimension < 10)
    {
        _textItem->setPos((_position.GetX()+0.4)*PuzzleSideLength, (_position.GetY()+0.3)*PuzzleSideLength);
    }
    else
    {
        _textItem->setPos((_position.GetX()+0.3)*PuzzleSideLength, (_position.GetY()+0.3)*PuzzleSideLength);
    }
}

/**
 * \brief Metoda zwracająca pozycję kwadratu
 *
 * \return pozycja kwadratu
 *
 */
Position TileItem::GetPosition() const
{
    return _position;
}

/**
 * \brief Metoda zwracająca wartość
 *
 * \return wartość
 *
 */
int TileItem::GetValue() const
{
    return _value;
}
