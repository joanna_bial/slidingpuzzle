#ifndef DIGITALCLOCK_H
#define DIGITALCLOCK_H

#include <QLCDNumber>
#include <QTimer>

/**
 * \brief Zegar cyfrowy pokazujący mijający czas w trybie gry
 *
 * Mierzy upływajcy czas, startuje i zatrzymuje timer, ustawia początkowy czas
 *
 */
class DigitalClock : public QLCDNumber
{
    Q_OBJECT

public:
    DigitalClock(QWidget *parent = nullptr);
    void Start();
    void Stop();
    int GetElapsedTime() const;
    void SetElapsedTime(int time);

private slots:
    void showTime()
    {
        if (_shouldCount)
        {
            _elapsedTime++;
        }

        display(QString::number(_elapsedTime));
    }

private:
    QTimer* timer;
    int _elapsedTime;
    bool _shouldCount;
};

#endif // DIGITALCLOCK_H
