#include "Score.h"

/**
 * \brief Konstruktor
 *
 * Tworzy obiekt klasy Score
 *
 */
Score::Score()
{
}

/**
 * \brief Konstruktor parametryczny
 *
 * Tworzy obiekt klasy Score
 * \param playerName nazwa gracza
 * \param gameName nazwa gry
 * \param moves liczba ruchów
 * \param time czas
 * \param dimension wymiar planszy
 *
 */
Score::Score(std::string playerName, std::string gameName, int moves, int time, int dimension)
    : _playerName(playerName),
      _gameName(gameName),
      _moves(moves),
      _time(time),
      _dimension(dimension)
{
}

/**
 * \brief Zwraca string zawierający wynik
 *
 * \return string
 *
 */
std::string Score::GetString() const
{
    std::string content;

    content+= "Score:\n";

    content+= "PlayerName:";
    content += _playerName;
    content += "\n";

    content+= "GameName:";
    content += _gameName;
    content += "\n";

    content+= "Time:";
    content += std::to_string(_time);
    content += "\n";

    content+= "Moves:";
    content += std::to_string(_moves);
    content += "\n";

    content+= "Dimension:";
    content += std::to_string(_dimension);

    return content;
}

/**
 * \brief Zwraca nazwę gracza
 *
 * \return nazwa gracza
 *
 */
std::string Score::GetPlayerName() const
{
    return _playerName;
}

/**
 * \brief Zwraca nazwę gry
 *
 * \return nazwa gry
 *
 */
std::string Score::GetGameName() const
{
    return _gameName;
}

/**
 * \brief Zwraca liczbę wykonanych ruchów
 *
 * \return liczba wykonanych ruchów
 *
 */
int Score::GetMoves() const
{
    return _moves;
}

/**
 * \brief Zwraca czas gry
 *
 * \return czas gry
 *
 */
int Score::GetTime() const
{
    return _time;
}

/**
 * \brief Zwraca wymiar planszy przypisanej do wyniku
 *
 * \return wymiar
 *
 */
int Score::GetDimension() const
{
    return _dimension;
}
