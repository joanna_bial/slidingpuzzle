#include <QSignalMapper>
#include <QComboBox>
#include <QStringListModel>

#include "NewGamePage.h"
#include "GamePage.h"
#include "GameParametersLoaderSaver.h"

/**
 * \brief Konstruktor parametryczny
 *
 * Tworzy obiekt klasy NewGamePage
 * \param parent widget wywołujący konstruktor
 * \param stackedLayout layout przechowujący informacje o pozostałych widgetach
 *
 */
NewGamePage::NewGamePage(QWidget *parent, QStackedLayout *const stackedLayout)
    : QWidget(parent),
      _stackedLayout(stackedLayout),
      _vboxLayout(new QVBoxLayout),
      _gridLayout(new QGridLayout),
      _playerNameLineEdit(new QLineEdit),
      _gameNameLineEdit(new QLineEdit),
      _dimensionComboBox(new QComboBox),
      _playButton(new QPushButton(tr("PLAY"))),
      _goToMenuButton(new QPushButton(tr("MENU"))),
      _errorLabel(new QLabel)
{
    InsertNewGameFormLayout();
    InsertPlayButton();
    InsertGoToMenuButton();

    _vboxLayout->setContentsMargins(300, 0, 300, 250);
    setLayout(_vboxLayout);
}

void NewGamePage::InsertNewGameFormLayout()
{
    QLabel *playerNameLabel = new QLabel(tr("Nickname of the player:"));
    QLabel *gameNameLabel = new QLabel(tr("Name for the new game:"));
    QLabel *dimensionLabel = new QLabel(tr("Dimension:"));

    playerNameLabel->setBuddy(_playerNameLineEdit);
    gameNameLabel->setBuddy(_gameNameLineEdit);
    dimensionLabel->setBuddy(_dimensionComboBox);

    QStringList items;
    items << tr("3") << tr("4") << tr("5") << tr("6") << tr("7") << tr("8") << tr("9") << tr("10") ;
    QStringListModel *dimensionModel = new QStringListModel(items);

    _dimensionComboBox->setModel(dimensionModel);
    _dimensionComboBox->setFixedHeight(30);

    playerNameLabel->setFont(QFont("Times", 18));
    _playerNameLineEdit->setFont(QFont("Times", 18));

    gameNameLabel->setFont(QFont("Times", 18));
    _gameNameLineEdit->setFont(QFont("Times", 18));

    dimensionLabel->setFont(QFont("Times", 18));
    _dimensionComboBox->setFont(QFont("Times", 18));

    _gridLayout->addWidget(playerNameLabel, 0, 0, 1, 1);
    _gridLayout->addWidget(_playerNameLineEdit, 0, 1, 1, 1);

    _gridLayout->addWidget(gameNameLabel, 1, 0, 1, 1);
    _gridLayout->addWidget(_gameNameLineEdit, 1, 1, 1, 1);

    _gridLayout->addWidget(dimensionLabel, 2, 0, 1, 1);
    _gridLayout->addWidget(_dimensionComboBox, 2, 1, 1, 1);

    _vboxLayout->insertWidget(0, _errorLabel);
    _vboxLayout->insertLayout(1, _gridLayout);

    connect(_playerNameLineEdit, &QLineEdit::textChanged, this, &NewGamePage::enableDisablePlayButton);
    connect(_gameNameLineEdit, &QLineEdit::textChanged, this, &NewGamePage::enableDisablePlayButton);
    connect(_gameNameLineEdit, &QLineEdit::textChanged, this, &NewGamePage::changeErrorMessage);
}

void NewGamePage::InsertPlayButton()
{
    _vboxLayout->insertWidget(3, _playButton);
    _playButton->setFixedSize(200, 60);
    _playButton->setFont(QFont("Times", 20, QFont::Bold));
    _playButton->setEnabled(false);
    connect(_playButton, &QPushButton::clicked, this, &NewGamePage::goToGame);
}

void NewGamePage::InsertGoToMenuButton()
{
    _vboxLayout->insertWidget(4, _goToMenuButton);
    _goToMenuButton->setFixedSize(200, 60);
    _goToMenuButton->setFont(QFont("Times", 20, QFont::Bold));
    connect(_goToMenuButton, &QPushButton::clicked, this, &NewGamePage::goToMenu);
}

/**
 * \brief Slot przekierowujący do menu
 *
*/
void NewGamePage::goToMenu()
{
    _games.clear();
    _stackedLayout->setCurrentIndex(MENU_INDEX);
}

/**
 * \brief Slot przekierowujący do gry
 *
 *
*/
void NewGamePage::goToGame()
{
    int dimension = _dimensionComboBox->currentText().toInt();
    std::string playerName = _playerNameLineEdit->text().toUtf8().constData();
    std::string gameName = _gameNameLineEdit->text().toUtf8().constData();

    auto* gameWidget = (GamePage*)(_stackedLayout->widget(GAME_INDEX));
    gameWidget->SetupNewGame(dimension, playerName, gameName);

    _games.clear();
    _stackedLayout->setCurrentIndex(GAME_INDEX);
}

/**
 * \brief  Slot zarządzający przyciskiem rozpoczynającym grę
 *
*/
void NewGamePage::enableDisablePlayButton()
{
    std::string playerName = _playerNameLineEdit->text().toUtf8().constData();
    std::string gameName = _gameNameLineEdit->text().toUtf8().constData();

    if (playerName == "" || gameName == "")
    {
        _playButton->setEnabled(false);
    }
    else
    {
        _playButton->setEnabled(true);
    }
}

/**
 * \brief Slot zarządzający tekstem z inforamcją o błędzie kontroli podanych danych
 *
*/
void NewGamePage::changeErrorMessage()
{
    _errorLabel->setText("");
    std::string gameName = _gameNameLineEdit->text().toUtf8().constData();
    _playButton->setEnabled(true);

    for (auto game: _games)
    {
        if (game.GameName == gameName)
        {
            _errorLabel->setText("Game already exists");
            _playButton->setEnabled(false);
            break;
        }
    }
}

/**
 * \brief Slot ładujący zapisane gry
 *
*/
void NewGamePage::loadGames()
{
    GameParametersLoaderSaver gameParametersLoaderSaver;
    _games = gameParametersLoaderSaver.Load();
}
