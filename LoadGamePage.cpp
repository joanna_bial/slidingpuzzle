#include <QVBoxLayout>
#include <QLineEdit>

#include "LoadGamePage.h"
#include "GameParametersLoaderSaver.h"

/**
 * \brief Konstruktor parametryczny
 *
 * Tworzy obiekt klasy LoadGamePage
 * \param parent widget wywołujący konstruktor
 * \param stackedLayout layout przechowujący informacje o pozostałych widgetach
 *
 */
LoadGamePage::LoadGamePage(QWidget *parent, QStackedLayout *const stackedLayout)
    : QWidget(parent),
      _stackedLayout(stackedLayout),
      _vboxLayout(new QVBoxLayout),
      _gamesComboBox(new QComboBox),
      _playerNameLineEdit(new QLineEdit),
      _playButton(new QPushButton(tr("PLAY")))
{
    QLabel *playerNameLabel = new QLabel(tr("Enter the player's name:"));
    playerNameLabel->setBuddy(_playerNameLineEdit);
    playerNameLabel->setFont(QFont("Times", 18));
    _playerNameLineEdit->setFont(QFont("Times", 18));

    QHBoxLayout* hboxlayout = new QHBoxLayout;
    hboxlayout->addWidget(playerNameLabel);
    hboxlayout->addWidget(_playerNameLineEdit);
    connect(_playerNameLineEdit, &QLineEdit::textChanged, this, &LoadGamePage::enableDisablePlayButton);

    _gamesComboBox->setFixedHeight(30);

    _vboxLayout->insertLayout(0, hboxlayout);
    _vboxLayout->insertWidget(1, _gamesComboBox);
    InsertButtons();

    _vboxLayout->setContentsMargins(300, 300, 300, 300);
    setLayout(_vboxLayout);
}

bool LoadGamePage::LoadGames()
{
    GameParametersLoaderSaver gameParametersLoaderSaver;
    _games = gameParametersLoaderSaver.Load();
    return _games.size() != 0;
}

void LoadGamePage::InsertButtons()
{
    _playButton->setFixedSize(200, 60);
    _playButton->setFont(QFont("Times", 20, QFont::Bold));
    connect(_playButton, &QPushButton::clicked, this, &LoadGamePage::goToGame);
    std::string playerName = _playerNameLineEdit->text().toUtf8().constData();
    _playButton->setEnabled(false);

    QPushButton *const goToMenuButton = new QPushButton(tr("MENU"), this);
    goToMenuButton->setFont(QFont("Times", 20, QFont::Bold));
    goToMenuButton->setFixedSize(200, 60);
    connect(goToMenuButton, &QPushButton::clicked, this, &LoadGamePage::goToMenu);

    _vboxLayout->insertWidget(2, _playButton);
    _vboxLayout->insertWidget(3, goToMenuButton);
}

GameParameters LoadGamePage::FindGameParameters(std::string name)
{
    for (auto game: _games)
    {
        if (game.GameName == name)
        {
            return game;
        }
    }

    return GameParameters("", std::vector<int>());
}

/**
 * \brief Slot przekierowujący do menu
 *
*/
void LoadGamePage::goToMenu()
{
    _games.clear();
    _stackedLayout->setCurrentIndex(MENU_INDEX);
}

/**
 * \brief Slot przekierowujący do okna gry
 *
*/
void LoadGamePage::goToGame()
{
    std::string selectedGameString = _gamesComboBox->currentText().toStdString();
    std::string gameName;

    auto it = selectedGameString.begin();
    while(*it != '(')
    {
        gameName += *it;
        it++;
    }
    gameName.pop_back();

    auto gameParameters = FindGameParameters(gameName);
    std::string playerName = _playerNameLineEdit->text().toUtf8().constData();

    if (gameParameters.InitialBoardState.size() != 0)
    {
        auto* gameWidget = (GamePage*)(_stackedLayout->widget(GAME_INDEX));
        gameWidget->LoadGame(gameParameters, playerName);

        _games.clear();
        _stackedLayout->setCurrentIndex(GAME_INDEX);
    }
}

/**
 * \brief Slot łądujący stronę
 *
*/
void LoadGamePage::loadPage()
{

    if (LoadGames())
    {
        QStringList items;

        for (auto game : _games)
        {
            items << QString::fromStdString(game.GameName + " (Dimension: " + std::to_string(game.Dimension) + ")");
        }

        QStringListModel *gamesModel = new QStringListModel(items);
        _gamesComboBox->setModel(gamesModel);
        _gamesComboBox->setFont(QFont("Times", 18));
    }
}

/**
 * \brief Slot zarządzający przyciskiem rozpoczynającym grę
 *
*/
void LoadGamePage::enableDisablePlayButton()
{
    std::string playerName = _playerNameLineEdit->text().toUtf8().constData();

    if (playerName == "")
    {
        _playButton->setEnabled(false);
    }
    else
    {
         _playButton->setEnabled(true);
    }
}
