#include "GameParametersLoaderSaver.h"

/**
 * \brief Konstruktor
 *
 * Tworzy obiekt klasy GameParametersLoaderSaver
 * Ustawia nazwę pliku na "games.txt"
 *
 */
GameParametersLoaderSaver::GameParametersLoaderSaver()
    : LoaderSaver("games.txt")
{
}

/**
 * \brief  Metoda zapisująca parametry gry
 *
 * \param game parametry gry, które zostaną zapisane
 * \return informacja o powodzeniu operacji
 *
 */
bool GameParametersLoaderSaver::Save(GameParameters gameParameters)
{
    auto scoreString = Serialize(gameParameters);
    return WriteFile(scoreString);
}

/**
 * \brief  Metoda wczytująca parametry gry
 *
 * \return wczytane parametry gry
 *
 */
std::vector<GameParameters> GameParametersLoaderSaver::Load()
{
    auto lines = ReadFile();
    return Deserialize(lines);
}

/**
 * \brief  Metoda serializująca parametry gry
 *
 * \param game parametry gry, które zostaną zapisane
 * \return string
 *
 */
std::string GameParametersLoaderSaver::Serialize(GameParameters gameParameters)
{
    return gameParameters.GetString();
}

/**
 * \brief Metoda deserializująca parametry gry
 *
 * \param lines linie odczytane z pliku
 * \return game
 *
 */
std::vector<GameParameters> GameParametersLoaderSaver::Deserialize(std::vector<std::string> lines)
{
    std::vector<GameParameters> games;

    std::vector<int> initialBoardState;
    std::string gameName;

    for (auto line : lines)
    {
        std::string flag = "";
        auto it = line.begin();
        while (*it != ':')
        {
            flag += *it;
            it++;
        }

        it++;
        if (flag == "InitialBoardState")
        {
            initialBoardState = GetVectorFromLine(it, line);
        }
        else if (flag == "GameName")
        {
            gameName = GetStringFromLine(it, line);
        }
        else if (flag == "end")
        {
            games.push_back(GameParameters(gameName, initialBoardState));
        }
    }

    return games;
}

bool GameParametersLoaderSaver::ShouldClear() const
{
    return false;
}
