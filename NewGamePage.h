#ifndef NEWGAMEPAGE_H
#define NEWGAMEPAGE_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QStackedLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLineEdit>
#include <QComboBox>

#include "MainWindow.h"
#include "GamePage.h"
#include "Game.h"

/**
 * \brief Klasa wyświetlająca stronę z tworzeniem nowej gry
 *
 * Obsługuje zdarzenia występujące w oknie tworzenia nowej gry
 *
 */
class NewGamePage : public QWidget
{
    Q_OBJECT

public:
    explicit NewGamePage(QWidget *parent, QStackedLayout *const stackedLayout);

public slots:
    void goToMenu();
    void goToGame();
    void enableDisablePlayButton();
    void changeErrorMessage();
    void loadGames();

private:
    QStackedLayout *const _stackedLayout;
    QVBoxLayout *const _vboxLayout;
    QGridLayout* const _gridLayout;
    QLineEdit* const _playerNameLineEdit;
    QLineEdit* const _gameNameLineEdit;
    QComboBox* const _dimensionComboBox;
    QPushButton *const _playButton;
    QPushButton *const _goToMenuButton;
    QLabel *const _errorLabel;
    std::vector<GameParameters> _games;

    void InsertNewGameFormLayout();
    void InsertPlayButton();
    void InsertGoToMenuButton();
};
#endif // NEWGAMEPAGE_H
