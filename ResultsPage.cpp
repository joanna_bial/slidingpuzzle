#include <QPushButton>

#include "ResultsPage.h"

/**
 * \brief Konstruktor parametryczny
 *
 * Tworzy obiekt klasy ResultsPage
 * \param parent widget wywołujący konstruktor
 * \param stackedLayout layout przechowujący informacje o pozostałych widgetach
 *
 */
ResultsPage::ResultsPage(QWidget *parent, QStackedLayout *const stackedLayout)
    : QWidget(parent),
      _stackedLayout(stackedLayout),
      _vboxLayout(new QVBoxLayout)
{
    _stackedLayout->setCurrentIndex(RESULTS_INDEX);

    std::string titleText = "WELL DONE!\n";
    titleText += "Results:";

    auto titleLabel = new QLabel(this);
    titleLabel->setFont(QFont("Times", 24));

    _resultsLabel = new QLabel(this);
    _resultsLabel->setFont(QFont("Times", 18));

    titleLabel->setText(QString::fromStdString(titleText));

    QPushButton *const goToMenuButton = new QPushButton((tr("MENU")));
    connect(goToMenuButton, &QPushButton::clicked, this, &ResultsPage::goToMenu);

    _vboxLayout->addWidget(titleLabel);
    _vboxLayout->addWidget(_resultsLabel);
    _vboxLayout->addWidget(goToMenuButton);

    _vboxLayout->setContentsMargins(400, 150, 400, 150);
    setLayout(_vboxLayout);
}

/**
 * \brief Slot ładujący stronę
 *
 */
void ResultsPage::showResults(Score score)
{
    _stackedLayout->setCurrentIndex(RESULTS_INDEX);

    std::string resultsText = "Player: " + score.GetPlayerName() + "\n";
    resultsText += "Game: " + score.GetGameName() + "\n";
    resultsText += "Dimension: " + std::to_string(score.GetDimension()) + "\n";
    resultsText += "Moves: " + std::to_string(score.GetMoves()) + "\n";
    resultsText += "Time: " + std::to_string(score.GetTime()) + "s \n";

    _resultsLabel->setText(QString::fromStdString(resultsText));
}

/**
 * \brief Slot przekierowujący do menu
 *
 */
void ResultsPage::goToMenu()
{
    emit gameFinished();
    _stackedLayout->setCurrentIndex(MENU_INDEX);
}
