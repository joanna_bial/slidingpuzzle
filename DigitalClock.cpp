#include <QTimer>
#include <QTime>
#include <QElapsedTimer>
#include "DigitalClock.h"

/**
 * \brief Konstruktor
 *
 * Tworzy obiekt klasy DigitalClock
 * Ustawia styl i rozmiar obiektu
 * Ustawia elapsedTime na wartość 0
 * Uruchamia timer, ale bez zwiększania elapsedTime
 *
 */
DigitalClock::DigitalClock(QWidget *parent)
    : QLCDNumber(parent),
      _elapsedTime(0),
      _shouldCount(false)
{
    setSegmentStyle(Filled);
    setFixedSize(100,50);
    timer = new QTimer(this);
    timer->setInterval(1000);
    connect(timer, SIGNAL(timeout()), this, SLOT(showTime()));
    timer->start();
}

/**
 * \brief  Metoda uruchamiająca odliczanie (zwiększanie wartości elapsedTime)
 *
 * zmienia wartość shouldCount na true
 *
 */
void DigitalClock::Start()
{
    _shouldCount = true;
}

/**
 * \brief Metoda zatrzymująca odliczanie (zwiększanie wartości elapsedTime)
 *
 * zmienia wartość shouldCount na false
 *
 */
void DigitalClock::Stop()
{
    _shouldCount = false;
}

/**
 * \brief Metoda zwracająca czas, który upłynął od uruchomienia
 *
 * \return czas, który upłynął (elapsedTime)
 *
 */
int DigitalClock::GetElapsedTime() const
{
    return _elapsedTime;
}

/**
 * \brief Metoda ustawiająca czas zmienną określającą ilość upłyniętego czasu
 *
 * Ustawia elapsedTime na wartość time
 * \param time, czas określony w sekundach
 *
 */
void DigitalClock::SetElapsedTime(int time)
{
    _elapsedTime = time;
}
