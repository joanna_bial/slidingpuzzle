#include <QVBoxLayout>
#include <QHeaderView>

#include "ScoresPage.h"
#include "ScoreLoaderSaver.h"

/**
 * \brief Konstruktor parametryczny
 *
 * Tworzy obiekt klasy ScoresPage
 * \param parent widget wywołujący konstruktor
 * \param stackedLayout layout przechowujący informacje o pozostałych widgetach
 *
 */
ScoresPage::ScoresPage(QWidget *parent, QStackedLayout *const stackedLayout)
    : QWidget(parent),
      _stackedLayout(stackedLayout),
      _goToMenuButton(new QPushButton(tr("MENU"), this))
{
    _table = new QTableView;
    _model = new QStandardItemModel;

    _model->setColumnCount(5);
    _model->setRowCount(_scores.size());

    QStringList labels {"Game", "Player", "Dimension", "Moves", "Time"};
    _model->setHorizontalHeaderLabels(labels);
    _table->setModel(_model);
    _table->verticalHeader()->hide();

    QVBoxLayout *const layout = new QVBoxLayout(this);
    QLabel *const message = new QLabel(tr("Scores:"));
    message->setFont(QFont("Times", 18));
    _goToMenuButton->setFixedSize(QSize(70, 30));

    layout->addWidget(message);
    layout->addWidget(_table);
    layout->addWidget(_goToMenuButton);

    connect(_goToMenuButton, &QPushButton::clicked, this, &ScoresPage::goToMenu);
}

void ScoresPage::ReadScores()
{
    ScoreLoaderSaver loader;
    _scores = loader.Load();
}

/**
 * \brief Slot przekierowujący do menu
 *
 */
void ScoresPage::goToMenu()
{
    _scores.clear();
    _stackedLayout->setCurrentIndex(MENU_INDEX);
}

/**
 * \brief Slot ładujący stronę
 *
 */
void ScoresPage::loadPage()
{
    ReadScores();

    _model->setRowCount(_scores.size());

    int i = 0;
    for (auto score: _scores)
    {
        _model->setData(_model->index(i, 0), QString::fromStdString(score.GetGameName()));
        _model->setData(_model->index(i, 1), QString::fromStdString(score.GetPlayerName()));
        _model->setData(_model->index(i, 2), QString::number(score.GetDimension()));
        _model->setData(_model->index(i, 3), QString::number(score.GetMoves()));
        _model->setData(_model->index(i, 4), QString::number(score.GetTime()));
        i++;
    }

    _table->setMaximumWidth(_table->horizontalHeader()->sectionSize(0)*5+2);
    _table->show();
}
