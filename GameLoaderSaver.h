#ifndef UNFINISHEDGAMELOADERSAVER_H
#define UNFINISHEDGAMELOADERSAVER_H

#include <memory>

#include "LoaderSaver.h"
#include "Game.h"

/**
 * \brief Klasa obsugująca zapisanie i odczytanie gry dla konkretnego gracza
 *
 * Stworzona w celu dodania możliwości kontynuowanaia niezakończonej gry,
 * od momentu przerwania
 * Zapisuje i odczytuję stan początkowy i stan aktulny, nazwę gracza, liczbę wykonanych róchow,
 * czas, który upłynął od rozpoczęcia gry
 *
 */

class GameLoaderSaver : public LoaderSaver
{
public:
    GameLoaderSaver();
    std::unique_ptr<Game> Load();
    bool Save(Game game);
    std::string Serialize(Game game);
    std::unique_ptr<Game> Deserialize(std::vector<std::string> lines);

private:
    virtual bool ShouldClear() const override;
};

#endif // UNFINISHEDGAMELOADERSAVER_H
