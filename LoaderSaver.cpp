#include <iostream>
#include <fstream>

#include "LoaderSaver.h"

/**
 * \brief Konstruktor paramatryczny
 *
 * Tworzy obiekt klasy LoaderSaver
 * \param path ścieżka do folderu w którym zostanie zapisany/odczytany plik
 *
 */
LoaderSaver::LoaderSaver(std::string path)
    : _path(path)
{
}

/**
 * \brief Metoda zapisują tekst do pliku
 *
 * \param content tekst, który ma zostać zapisany
 *
 */
bool LoaderSaver::WriteFile(std::string content) const
{
    if (content.size() > 0)
    {
        if (ShouldClear())
        {
           ClearFile();
        }

        std::ofstream file;
        file.open(_path, std::ios::app);

        if (file.is_open())
        {
            file << content;
            file << "\nend:\n";
            file.close();

            return true;
        }
    }

    return false;
}

/**
 * \brief Metoda odczytująca zawartość pliku
 *
 * \return odczytany tekst
 *
 */
std::vector<std::string> LoaderSaver::ReadFile() const
{
    std::ifstream file;
    std::string line;
    std::vector<std::string> lines;

    file.open(_path);

    if (file.is_open())
    {
        while (std::getline(file, line))
        {
          lines.push_back(line);
        }

        file.close();
    }

    return lines;
}

/**
 * \brief Metoda usuwająca zawartość pliku
 *
 * \return informacja o powodzeniu operacji
 *
 */
bool LoaderSaver::ClearFile() const
{
    std::ofstream file;
    file.open(_path, std::ofstream::out | std::ofstream::trunc);

    if (file.is_open())
    {
        file.close();

        return true;
    }

    return false;
}

/**
 * \brief Metoda konwertująca linię pliku na wektor
 *
 * \param it iterator
 * \param line linia pliku
 * \return wektor wartości z linii
 *
 */
std::vector<int> LoaderSaver::GetVectorFromLine(std::string::iterator it, std::string& line)
{
    std::vector<int> vec;

    while (it != line.end())
    {
        std::string numStr;

        if(*it != ',')
        {
            numStr = *it;
            vec.push_back(std::stoi(numStr));
        }

        it++;
    }

    return vec;
}

/**
 * \brief Metoda konwertująca linię pliku na int
 *
 * \param it iterator
 * \param line linia pliku
 * \return int
 *
 */
int LoaderSaver::GetIntFromLine(std::string::iterator it, std::string& line)
{
    int value;
    std::string valueStr;

    while (it != line.end())
    {
        valueStr += *it;
        value = std::stoi(valueStr);
        it++;
    }

    return value;
}

/**
 * \brief Metoda konwertująca linię pliku na string
 *
 * \param it iterator
 * \param line linia pliku
 * \return string
 *
 */
std::string LoaderSaver::GetStringFromLine(std::string::iterator it, std::string &line)
{
    std::string text;
    while (it != line.end())
    {
        text += *it;
        it++;
    }

    return text;
}
