#include "GameLoaderSaver.h"

/**
 * \brief Konstruktor
 *
 * Tworzy obiekt klasy GameLoaderSaver
 * Ustawia nazwę pliku na "unfinishes_game.txt"
 *
 */
GameLoaderSaver::GameLoaderSaver()
    : LoaderSaver("unfinishes_game.txt")
{
}

/**
 * \brief  Metoda wczytująca grę
 *
 * \return wczytana gra
 *
 */
bool GameLoaderSaver::Save(Game game)
{
    auto gameString = Serialize(game);
    return WriteFile(gameString);
}

/**
 * \brief  Metoda zapisująca grę
 *
 * \param game gra, która zostanie zapisana
 * \return informacja o powodzeniu operacji
 *
 */
std::unique_ptr<Game> GameLoaderSaver::Load()
{
    auto lines = ReadFile();
    return (lines.size() > 0) ? Deserialize(lines) : nullptr;
}

/**
 * \brief  Metoda serializująca grę
 *
 * \param game gra, która zostanie zapisana
 * \return string
 *
 */
std::string GameLoaderSaver::Serialize(Game game)
{
    return game.GetString();
}

/**
 * \brief  Metoda deserializująca grę
 *
 * \param lines linie odczytane z pliku
 * \return game
 *
 */
std::unique_ptr<Game> GameLoaderSaver::Deserialize(std::vector<std::string> lines)
{
    std::vector<int> initialBoardState;
    std::vector<int> currentBoardState;
    int time;
    int moves;
    std::string playerName;
    std::string gameName;

    for (auto line : lines)
    {
        std::string flag = "";
        auto it = line.begin();
        while (*it != ':')
        {
            flag += *it;
            it++;
        }

        it++;
        if (flag == "InitialBoardState")
        {
            initialBoardState = GetVectorFromLine(it, line);
        }
        else if (flag == "CurrentBoardState")
        {
            currentBoardState = GetVectorFromLine(it, line);
        }
        else if (flag == "Time")
        {
            time = GetIntFromLine(it, line);
        }
        else if (flag == "Moves")
        {
            moves = GetIntFromLine(it, line);
        }
        else if (flag == "PlayerName")
        {
            playerName = GetStringFromLine(it, line);
        }
        else if (flag == "GameName")
        {
            gameName = GetStringFromLine(it, line);
        }
    }

    GameParameters parameters(gameName, initialBoardState);

    return std::make_unique<Game>(parameters, currentBoardState, time, moves, playerName);
}

bool GameLoaderSaver::ShouldClear() const
{
    return true;
}
