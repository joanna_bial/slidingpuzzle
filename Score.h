#ifndef SCORE_H
#define SCORE_H

#include <string>

/**
 * \brief Klasa opisująca wynik gry
 *
 * Zawiera informacje o nazwie gracza, nazwie gry,
 * liczbie wykonanych ruchów, czasie gry, wymiarze planszy
 *
 */
class Score
{
public:
    Score();
    Score(std::string playerName, std::string gameName, int moves, int time, int dimension);
    std::string GetString() const;
    std::string GetPlayerName() const;
    std::string GetGameName() const;
    int GetMoves() const;
    int GetTime() const;
    int GetDimension() const;

private:
    std::string _playerName;
    std::string _gameName;
    int _moves;
    int _time;
    int _dimension;
};

#endif // SCORE_H
