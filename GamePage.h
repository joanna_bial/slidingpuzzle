#ifndef GAMEPAGE_H
#define GAMEPAGE_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QStackedLayout>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
#include <QPen>
#include <QMouseEvent>
#include <QGraphicsView>
#include <QTimer>
#include <memory>

#include "MainWindow.h"
#include "TileItem.h"
#include "DigitalClock.h"
#include "GameController.h"
#include "Score.h"
#include "GameLoaderSaver.h"
#include "ScoreLoaderSaver.h"

/**
 * \brief Klasa wyświetlająca stronę z grą
 *
 * Obsługuje zdarzenia występujące w oknie gry
 *
 */
class GamePage : public QWidget
{
    Q_OBJECT

public:
    explicit GamePage(QWidget *parent, QStackedLayout *const stackedLayout);
    void SetupNewGame(int dimension, std::string playerName, std::string gameName);
    void ContinueGame(std::unique_ptr<Game> game);
    void LoadGame(GameParameters parameters, std::string playerName);

signals:
    void gameNotFinished();
    void gameFinished(Score score);

public slots:
    void start();
    void stop();
    void reset();
    void moveBackward();
    void moveForward();
    void goToMenu();
    void goToResults();

private:
    QStackedLayout *const _stackedLayout;
    QHBoxLayout *const _horizonatalLayout;
    QPushButton* _startButton;
    QPushButton* _stopButton;
    QPushButton* _backwardButton;
    QPushButton* _forwardButton;
    QLabel* _movesLabel;
    QGraphicsScene* _scene;
    QGraphicsView* _view;
    DigitalClock* _clock;
    std::vector<TileItem> _tiles;
    std::unique_ptr<GameController> _controller;
    bool _started;

    void InsertScene();
    void DrawBoard();
    void ClearBoard();
    void InsertRightPanel();
    void ReloadRightPanel();
    QHBoxLayout* CreateNarrowsButtonsLayout();
    QHBoxLayout* CreateStartStopButtonsLayout();
    QVBoxLayout* CreateRightPanelButtons();
    void HandleMousePressedOnScene(QPointF mousePoint);
    virtual void mousePressEvent(QMouseEvent *event) override;
    void GoToResults();
    void ClosePage();
};

#endif // GAMEPAGE_H
