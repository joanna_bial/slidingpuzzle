#ifndef LOADGAMEPAGE_H
#define LOADGAMEPAGE_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QStackedLayout>
#include <QComboBox>
#include <QStringListModel>
#include <QLineEdit>

#include "MainWindow.h"
#include "Game.h"
#include "GamePage.h"

/**
 * \brief Klasa wyświetlająca stronę z wyborem jednej z zapisanych gier
 *
 * Obsługuje zdarzenia występujące w oknie wyboru gry
 *
 */
class LoadGamePage : public QWidget
{
    Q_OBJECT

public:
    explicit LoadGamePage(QWidget *parent, QStackedLayout *const stackedLayout);

public slots:
    void goToMenu();
    void goToGame();
    void loadPage();
    void enableDisablePlayButton();

private:
    QStackedLayout *const _stackedLayout;
    QVBoxLayout *const _vboxLayout;

    std::vector<GameParameters> _games;
    QComboBox* const _gamesComboBox;
    QLineEdit* const _playerNameLineEdit;
    QPushButton* const _playButton;

    bool LoadGames();
    void InsertComboBox();
    void InsertButtons();
    GameParameters FindGameParameters(std::string name);
};

#endif // LOADGAMEPAGE_H
