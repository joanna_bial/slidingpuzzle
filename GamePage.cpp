#include <QHBoxLayout>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
#include <QSignalMapper>

#include "GamePage.h"
#include "TileItem.h"
#include "Position.h"
#include "GameLoaderSaver.h"
#include "Score.h"

/**
 * \brief Konstruktor parametryczny
 *
 * Tworzy obiekt klasy GamePage
 * \param parent widget wywołujący konstruktor
 * \param stackedLayout layout przechowujący informacje o pozostałych widgetach
 *
 */
GamePage::GamePage(QWidget *parent, QStackedLayout *const stackedLayout)
    : QWidget(parent),
      _stackedLayout(stackedLayout),
      _horizonatalLayout(new QHBoxLayout(this)),
      _scene(new QGraphicsScene),
      _view(new QGraphicsView(_scene)),
      _clock(new DigitalClock(this)),
      _started(false)
{
    _horizonatalLayout->addWidget(_view);
    InsertRightPanel();    
}

/**
 * \brief Metoda otwierająca okno z nową grę
 *
 * Wstawia planszę
 * Przeładowuje prawy panel
 * \param dimension wymiar planszy
 * \param playerName nazwa gracza
 * \param gameName nazwa gry
 *
 */
void GamePage::SetupNewGame(int dimension, std::string playerName, std::string gameName)
{
    _started = false;
    _clock->SetElapsedTime(0);
    _controller = std::make_unique<GameController>(dimension, playerName, gameName);
    InsertScene();
    ReloadRightPanel();
}

/**
 * \brief Metoda uruchamiająca kontynuowanie gry
 *
 * Wstawia planszę
 * Przeładowuje prawy panel
 * Inicjalizuje _controller kontynuowaną grą
 * \param game kontynuowana gra
 *
 */
void GamePage::ContinueGame(std::unique_ptr<Game> game)
{
    _started = true;
    _clock->SetElapsedTime(game->GetElapsedTime());
    _controller = std::make_unique<GameController>(std::move(game));
    InsertScene();
    ReloadRightPanel();
}

/**
 * \brief Metoda wczytująca grę na podstawie jej parametrów statycznych
 *
 * Wstawia planszę
 * Przeładowuje prawy panel
 * Inicjalizuje _controller parametrami statycznymi
 * \param parameters parametry gry - stan początkowy, nazwa, wymiar
 * \param playerName nazwa gracza
 *
 */
void GamePage::LoadGame(GameParameters parameters, std::string playerName)
{
    _started = false;
    _clock->SetElapsedTime(0);
    _controller = std::make_unique<GameController>(parameters, playerName);
    InsertScene();
    ReloadRightPanel();
}

void GamePage::InsertScene()
{
    int puzzleSideLength = static_cast<int>(700/_controller->GetDimension());

    TileItem::PuzzleSideLength = puzzleSideLength;
    TileItem::Dimension = _controller->GetDimension();

    int boardSideLength = puzzleSideLength*_controller->GetDimension();

    _view->setMinimumSize(boardSideLength + 2, boardSideLength + 2);
    _view->setMaximumSize(boardSideLength + 2, boardSideLength + 2);

    _scene->setSceneRect(0, 0, boardSideLength, boardSideLength);
}

void GamePage::DrawBoard()
{
    ClearBoard();

    auto values = _controller->GetValues();

    for (auto value: values)
    {
        if (value.second != 0)
        {
            TileItem puzzle = TileItem(value.second, _scene, value.first);
            _tiles.push_back(puzzle);
        }
    }
}

void GamePage::InsertRightPanel()
{
    auto verticalLayout = new QVBoxLayout;
    _horizonatalLayout->addLayout(verticalLayout);

    _movesLabel = new QLabel(this);
    _movesLabel->setFont(QFont("Times", 14));
    _movesLabel->setText("Moves: 0");
    _movesLabel->setFixedSize(QSize(150,30));

    verticalLayout->addLayout(CreateStartStopButtonsLayout());
    verticalLayout->addLayout(CreateNarrowsButtonsLayout());
    verticalLayout->addLayout(CreateRightPanelButtons());
    verticalLayout->addWidget(_clock);
    verticalLayout->addWidget(_movesLabel);

    verticalLayout->setContentsMargins(100,150,100,150);
}

void GamePage::ClearBoard()
{
    _scene->clear();
    _tiles.clear();
}

void GamePage::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        QPoint remapped =_view->mapFromParent( event->pos() );
        if ( _view->rect().contains( remapped ) )
        {
             QPointF mousePoint = _view->mapToScene(remapped);
             HandleMousePressedOnScene(mousePoint);
        }
    }
}

void GamePage::HandleMousePressedOnScene(QPointF mousePoint)
{
    for (auto& puzzle: _tiles)
    {
        if (puzzle.ContainsMousePosition(mousePoint))
        {
            if (_controller->MoveToBlank(puzzle.GetPosition()))
            {
                ClearBoard();
                DrawBoard();
                ReloadRightPanel();
            }
        }
    }

    if (_controller->IsFinished())
    {
        goToResults();
    }
}

void GamePage::ReloadRightPanel()
{
    _forwardButton->setEnabled(_controller->CanNavigateForward());
    _backwardButton->setEnabled(_controller->CanNavigateBackward());

    _movesLabel->setText(QString("Moves: " + QString::number(_controller->GetMoves())));
}

QHBoxLayout* GamePage::CreateNarrowsButtonsLayout()
{
    QPixmap pixmapForward("C:/Users/jooan/Desktop/studia/magisterskie/C++/SlidingPuzzle/move_forward_button.jpg");
    QIcon forwardButtonIcon(pixmapForward);
    _forwardButton = new QPushButton(this);
    _forwardButton->setIcon(forwardButtonIcon);
    _forwardButton->setIconSize(QSize(40,40));
    _forwardButton->setFixedSize(QSize(40,40));

    QPixmap pixmapBackward("C:/Users/jooan/Desktop/studia/magisterskie/C++/SlidingPuzzle/move_backward_button.jpg");
    QIcon backwardButtonIcon(pixmapBackward);
    _backwardButton = new QPushButton(this);
    _backwardButton->setIcon(backwardButtonIcon);
    _backwardButton->setIconSize(QSize(40,40));
    _backwardButton->setFixedSize(QSize(40,40));

    auto narrowsLayout = new QHBoxLayout;
    narrowsLayout->addWidget(_backwardButton);
    narrowsLayout->addWidget(_forwardButton);
    narrowsLayout->setContentsMargins(0,0,40,0);

    connect(_backwardButton, &QPushButton::clicked, this, &GamePage::moveBackward);
    connect(_forwardButton, &QPushButton::clicked, this, &GamePage::moveForward);

    return narrowsLayout;
}

QHBoxLayout* GamePage::CreateStartStopButtonsLayout()
{
    QPixmap pixmapStart("C:/Users/jooan/Desktop/studia/magisterskie/C++/SlidingPuzzle/play.jpg");
    QIcon startIcon(pixmapStart);
    _startButton = new QPushButton(this);
    _startButton->setIcon(startIcon);
    _startButton->setIconSize(QSize(40,40));
    _startButton->setFixedSize(QSize(40,40));
    connect(_startButton, &QPushButton::clicked, this, &GamePage::start);

    QPixmap pixmapStop("C:/Users/jooan/Desktop/studia/magisterskie/C++/SlidingPuzzle/stop.jpg");
    QIcon playIcon(pixmapStop);
    _stopButton = new QPushButton(this);
    _stopButton->setIcon(playIcon);
    _stopButton->setIconSize(QSize(40,40));
    _stopButton->setFixedSize(QSize(40,40));
    _stopButton->setDisabled(true);
    connect(_stopButton, &QPushButton::clicked, this, &GamePage::stop);

    auto startStopLayout = new QHBoxLayout;
    startStopLayout->addWidget(_startButton);
    startStopLayout->addWidget(_stopButton);
    startStopLayout->setContentsMargins(0,0,40,0);

    return startStopLayout;
}

QVBoxLayout* GamePage::CreateRightPanelButtons()
{
    auto resetButton = new QPushButton(tr("RESET"), this);
    resetButton->setFixedSize(QSize(100, 30));
    connect(resetButton, &QPushButton::clicked, this, &GamePage::reset);

    auto goToMenuButton = new QPushButton(tr("MENU"), this);
    goToMenuButton->setFixedSize(QSize(100, 30));
    connect(goToMenuButton, &QPushButton::clicked, this, &GamePage::goToMenu);

    auto buttonsLayout = new QVBoxLayout;
    buttonsLayout->addWidget(resetButton);
    buttonsLayout->addWidget(goToMenuButton);

    return buttonsLayout;
}

void GamePage::ClosePage()
{
    ClearBoard();
    _controller.reset();
    _startButton->setEnabled(true);
    _stopButton->setEnabled(false);
    _clock->Stop();
    _clock->SetElapsedTime(0);
}

/**
 * \brief Slot rozpoczynający grę
 *
 * Inicjalizuje _controller
 * Rozpoczyna mierzenie czasu
 * Przeładowuje prawy panel
 *
 */
void GamePage::start()
{
    if(!_started)
    {
        _controller->Start();
        _started = true;
    }

    _clock->Start();
    DrawBoard();
    _startButton->setEnabled(false);
    _stopButton->setEnabled(true);
    ReloadRightPanel();
}

/**
 * \brief Slot zatrzymujący grę
 *
 * Wyłącza widoczność komórek na planszy
 * Zatrzymuje zegar
 * Przeładowuje przyciski
 *
*/
void GamePage::stop()
{
    _clock->Stop();
    ClearBoard();
    _startButton->setEnabled(true);
    _stopButton->setEnabled(false);
}

/**
 * \brief Slot resetujący grę
 *
 * Resetuje kontroler
 * Przeładowuje planszę z grą
 * Przeładowuje prawy panel
 *
*/
void GamePage::reset()
{
    _controller->Reset();
    DrawBoard();
    ReloadRightPanel();
}

/**
 * \brief Slot cofający ruch
 *
 *
 * Przeładowuje planszę z grą
 * Przeładowuje prawy panel
 *
*/
void GamePage::moveBackward()
{
    if (_controller->NavigateBackward())
    {
        DrawBoard();
        ReloadRightPanel();
    }
}

/**
 * \brief Slot przesuwający pustą komórkę o ruch w przód
 *
 *
 * Przeładowuje planszę z grą
 * Przeładowuje prawy panel
 *
*/
void GamePage::moveForward()
{
    if (_controller->NavigateForward())
    {
        DrawBoard();
        ReloadRightPanel();
    }
}

/**
 * \brief Slot przekierowujący do menu
 *
 * Zapisuje grę w celu jej późniejszego kontynuowania
 *
*/
void GamePage::goToMenu()
{
    if (_controller->SaveGameCurrentState(_clock->GetElapsedTime()))
    {
        emit gameNotFinished();
    }

    ClosePage();
    _stackedLayout->setCurrentIndex(MENU_INDEX);
}

/**
 * \brief Slot przekierowujący do wyników gry po rozwiązaniu
 *
 * Wywołuje czyszczenie pliku z zapisaną kontynuowaną grą
 *
*/
void GamePage::goToResults()
{
    GameLoaderSaver gameLoaderSaver;
    gameLoaderSaver.ClearFile();

    Score score = Score(_controller->GetPlayerName(), _controller->GetGameName(),_controller->GetMoves(), _clock->GetElapsedTime(), _controller->GetDimension());
    ScoreLoaderSaver saver;
    saver.Save(score);

    ClosePage();

    emit gameFinished(score);
}
