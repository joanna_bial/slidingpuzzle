#ifndef GAME_H
#define GAME_H

#include <vector>
#include <string>
#include <math.h>
#include "Position.h"

/**
 * \brief Struktura opisująca parametry gry
 *
 * Jej celem jest zapisanie statycznych parametrów gry
 * Przechowuje nazwę gry, stan początkowy, wymiar
 *
 */

struct GameParameters
{
    const std::string GameName;
    const std::vector<int> InitialBoardState;
    const int Dimension;

    /**
     * \brief Konstruktor paramatryczny
     *
     * \param gameName nazwa gry
     * \param initialBoardState początkowy stan planszy
     *
     */

    //! Konstruktor paramatryczny
    /*!
      \param gameName nazwa gry
      \param initialBoardState początkowy stan planszy
    */
    GameParameters(std::string gameName, std::vector<int> initialBoardState)
        : GameName(gameName),
          InitialBoardState(initialBoardState),
          Dimension(sqrt(initialBoardState.size()))
    {
    }

    std::string GetString()
    {
        std::string content = "Game";
        content += "\n";

        content+= "InitialBoardState:";
        for (auto value : InitialBoardState)
        {
            content += std::to_string(value);
            content += ",";
        }
        content += "\n";

        content+= "GameName:";
        content += GameName;

        return content;
    }
};

/**
 * \brief Klasa opisująca grę
 *
 * Jej celem jest zapisanie statycznych i dynamicznych parametrów gry
 * Zawiera informacje o aktualnym stanie gry, początkowym stanie gry,
 * nazwie gracza, nazwie gry, liczbie wykonanych przez niego ruchów oraz o czasie,
 *  który upłynął od rozpoczęcia gry.
 *
 */
class Game
{
public:
    Game(GameParameters parameters, std::string playerName);
    Game(GameParameters parameters, std::vector<int> currentBoardState, int time, int moves, std::string playerName);
    std::vector<int> GetInitialBoardState() const;
    std::vector<int> GetCurentBoardState() const;
    int GetMoves() const;
    int GetElapsedTime() const;
    Position GetInitialBlankFieldPosition() const;
    Position GetCurrentBlankFieldPosition() const;
    std::string GetPlayerName() const;
    GameParameters GetParameters() const;
    std::string GetString() const;
    void Update(std::vector<int> currentBoardState, int time, int moves);

private:
    GameParameters _parameters;
    std::vector<int> _currentBoardState;
    std::string _playerName;
    int _time;
    int _moves;
};

#endif // GAME_H
