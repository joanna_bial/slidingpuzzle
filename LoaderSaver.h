#ifndef LOADERSAVER_H
#define LOADERSAVER_H

#include <string>
#include <vector>

/**
 * \brief Klasa obsugująca zapis i odczyt
 *
 */

class LoaderSaver
{
public:
    LoaderSaver(std::string path);
    bool ClearFile() const;

protected:
    bool WriteFile(std::string content) const;
    std::vector<std::string> ReadFile() const;
    std::vector<int> GetVectorFromLine(std::string::iterator it, std::string& line);
    int GetIntFromLine(std::string::iterator it, std::string& line);
    std::string GetStringFromLine(std::string::iterator it, std::string& line);
    virtual bool ShouldClear() const = 0;

private:
    std::string _path;
};

#endif // LOADERSAVER_H
