#ifndef RESULTSPAGE_H
#define RESULTSPAGE_H

#include <QWidget>
#include <QStackedLayout>
#include <QLabel>

#include "Score.h"
#include "MainWindow.h"

/**
 * \brief Klasa wyświetlająca stronę z wyświetlaniem wyniku zakończonej gry
 *
 * Obsługuje zdarzenia występujące w oknie wyświetlania wyniku zakończonej gry
 *
 */
class ResultsPage: public QWidget
{
    Q_OBJECT

public:
    explicit ResultsPage(QWidget *parent, QStackedLayout *const stackedLayout);

signals:
    void gameFinished();

public slots:
    void showResults(Score score);
    void goToMenu();

private:
    QStackedLayout *const _stackedLayout;
    QVBoxLayout *const _vboxLayout;
    QLabel* _resultsLabel;
    Score score;
};

#endif // RESULTSPAGE_H
