#ifndef POSITION_H
#define POSITION_H

/**
 * \brief Klasa przechowująca współrzędne punktu
 *
 * Zawiera współrzędne pionowową oraz poziomą
 * Implementuje przeciążenia operatorów =, <, ==
 *
 */

class Position
{
private:
    int _x;
    int _y;

public:
    Position();
    Position(int x, int y);

    void operator=(const Position& p);
    bool operator<(const Position& p) const;
    bool operator==(const Position& p) const;
    int GetX() const;
    int GetY() const;
};

#endif // POSITION_H
