#include "Position.h"

/**
 * \brief Konstruktor
 *
 * Tworzy obiekt klasy Position
 *  Ustawia _x na -1 i _y na -1
 *
 */
Position::Position() : _x(-1), _y(-1)
{
}

/**
 * \brief Konstruktor parametryczny
 *
 * \param x współrzędna ox
 * \param y współrzędna oy
 *
 */
Position::Position(int x, int y) : _x(x), _y(y)
{
}

void Position::operator=(const Position& p)
{
    _x = p.GetX();
    _y = p.GetY();
}

bool Position::operator<(const Position& p) const
{
    if (_y != p.GetY())
        return _y < p.GetY();
    else
        return _x < p.GetX();
}

bool Position::operator==(const Position& p) const
{

    return _x == p.GetX() && _y == p.GetY();
}

/**
 * \brief Metoda zwracająca wartość współrzędnej ox
 *
 * \return wartość współrzędnej poziomej
 *
 */
int Position::GetX() const
{
    return _x;
}

/**
 * \brief Metoda zwracająca wartość współrzędnej oy
 *
 * \return wartość współrzędnej pionowej
 *
 */
int Position::GetY() const
{
    return _y;
}
