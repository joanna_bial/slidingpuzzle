#ifndef SAVEDGAMELOADERSAVER_H
#define SAVEDGAMELOADERSAVER_H

#include "LoaderSaver.h"
#include "Game.h"

/**
 * \brief Klasa obsugująca zapisanie i odczytanie parametrów statycznych gry
 *
 * Zapisuje i odczytuję stan początkowy i nazwę gry
 * Stworzona w celu zagrania ponownie w to samo ułożenie
 *
 */
class GameParametersLoaderSaver : LoaderSaver
{
public:
    GameParametersLoaderSaver();
    std::vector<GameParameters> Load();
    bool Save(GameParameters gameParameters);
    std::string Serialize(GameParameters gameParameters);
    std::vector<GameParameters> Deserialize(std::vector<std::string> lines);

private:
    virtual bool ShouldClear() const override;
};

#endif // SAVEDGAMELOADERSAVER_H
