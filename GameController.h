#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <vector>

#include "Position.h"
#include "TileItem.h"
#include "Board.h"
#include "Game.h"

/**
 * \brief Klasa kontrolująca stan gry
 *
 * Odpowiada za wykonanie operacji dostępnych w oknie gry
 * Obsługuje zmiany pozycji pustych pozycji
 * Zatrzymanie gry
 * Cofnięcie pozycji i poruszenie w przód
 *
 */
class GameController
{
public:
    GameController(int dimension, std::string playerName, std::string gameName);
    GameController(std::unique_ptr<Game> game);
    GameController(GameParameters parameters, std::string playerName);
    void Start();
    void Mix(int dimension);
    void Reset();
    bool MoveToBlank(Position position);
    bool SaveGameCurrentState(int elapsedTime);
    bool SaveGame();
    std::map<Position, int> GetValues() const;
    std::vector<int> GetValuesVector() const;
    std::string GetValuesString() const;
    Position* GetPositionForValue(int value) const;
    int GetMoves() const;
    int GetDimension() const;
    std::string GetPlayerName() const;
    std::string GetGameName() const;
    bool IsFinished() const;
    bool NavigateForward();
    bool NavigateBackward();
    bool CanNavigateBackward() const;
    bool CanNavigateForward() const;

private:
    std::unique_ptr<Game> _game;
    Board<int> _board;
    int _moves;

    std::vector<Position> _blankFieldForwardPositions;
    std::vector<Position> _blankFieldBackwardPositions;

    std::vector<int> CreateValuesVector(int dimension);
};

#endif // GAMECONTROLLER_H
