#include "GameController.h"
#include "GameLoaderSaver.h"
#include "GameParametersLoaderSaver.h"

#include <memory>

/**
 * \brief Konstruktor paramatryczny
 *
 * Tworzy obiekt klasy GameController
 * \param dimension wymiar planszy
 * \param playerName nazwa gracza
 * \param gameName nazwa gry
 *
 */
GameController::GameController(int dimension, std::string playerName, std::string gameName)
    : _moves(0)
{
    _board = Board<int>(CreateValuesVector(dimension), Position(dimension-1, dimension-1));
    Mix(dimension);
    GameParameters parameters(gameName, GetValuesVector());
    _game = std::make_unique<Game>(parameters, playerName);
    SaveGame();
}

/**
 * \brief Konstruktor paramatryczny
 *
 * Tworzy obiekt klasy GameController
 * \param game gra, która zostanie wczytana
 *
 */
GameController::GameController(std::unique_ptr<Game> game)
    : _game(std::move(game)),
    _moves(_game->GetMoves())
{
    _board = Board<int>(_game->GetCurentBoardState(), _game->GetCurrentBlankFieldPosition());
}

/**
 * \brief Konstruktor paramatryczny
 *
 * Tworzy obiekt klasy GameController
 * \param parameters parametry gry, która zostanie rozpoczęta
 * \param playerName nazwa gracza
 *
 */
GameController::GameController(GameParameters parameters, std::string playerName)
    : _game(std::make_unique<Game>(parameters, playerName)),
    _moves(0)
{
    _board = Board<int>(_game->GetInitialBoardState(), _game->GetInitialBlankFieldPosition());

}

/**
 * \brief Metoda rozpoczynająca grę
 *
 * Ustawia liczbę wykonanych ruchów na 0
 * Czyśli listy z wcześniejszymi pozycjami
 *
 */
void GameController::Start()
{
    _moves = 0;
    _blankFieldBackwardPositions.clear();
    _blankFieldForwardPositions.clear();
}

/**
 * \brief Metoda mieszająca wartości w planszy
 *
 * Miesza wartości planszy w taki sposób, żeby gra była rozwiązywalna
 * \param dimension wymiar planszy
 *
 */
void GameController::Mix(int dimension)
{
    _blankFieldBackwardPositions.clear();
    _blankFieldBackwardPositions.clear();

    srand(time(NULL));

    for (int i = 0; i < 100 * dimension; i++)
    {
        auto neighbours =_board.GetBlankNeighbors();

        auto rand = std::rand()%neighbours.size();
        _board.MoveToBlank(neighbours[rand]);
    }
}

/**
 * \brief Metoda resetująca stan gry
 *
 * Przywraca stan początkowy na podstawie parametrów statycznych gry
 *
 */
void GameController::Reset()
{
    int index = 0;

    for (auto val : _game->GetInitialBoardState())
    {
        if (val == 0)
        {
            break;
        }
        index++;
    }

    _board.InsertValues(_game->GetInitialBoardState(), _game->GetInitialBlankFieldPosition());
    _moves = 0;
    _blankFieldForwardPositions.clear();
    _blankFieldBackwardPositions.clear();
}

std::vector<int> GameController::CreateValuesVector(int dimension)
{
    std::vector<int> valuesSet;

    for (int i = 1; i < dimension * dimension; i++)
    {
        valuesSet.push_back(i);
    }

    valuesSet.push_back(0);

    return valuesSet;
}

/**
 * \brief Metoda zwracająca mapę wartości o pozycji na planszy
 *
 * \return mapa wartości
 *
 */
std::map<Position, int> GameController::GetValues() const
{
    return _board.GetValues();
}

/**
 * \brief Metoda zwracająca wektor wartości w kolejności występowania na planszy
 *
 * \return wektor wartości
 *
 */
std::vector<int> GameController::GetValuesVector() const
{
    std::vector<int> valuesVector;

    auto values = GetValues();
    for (auto value: values)
    {
        valuesVector.push_back(value.second);
    }

    return valuesVector;
}

/**
 * \brief Metoda zwracająca string wartości w kolejności występowania na planszy
 *
 * \return string wartości
 *
 */
std::string GameController::GetValuesString() const
{
    std::string valuesString;

    auto values = GetValues();
    for (auto value: values)
    {
        valuesString +=value.second;
    }

    return valuesString;
}

/**
 * \brief Metoda zwracająca pozycję o zadanej wartości
 *
 * \param value wartość
 * \return pozycja
 *
 */
Position* GameController::GetPositionForValue(int value) const
{
    return _board.GetPositionForValue(value);
}

/**
 * \brief Metoda zwracająca liczbę wykonanych ruchów
 *
 * \return liczba wykonanych ruchów
 *
 */
int GameController::GetMoves() const
{
    return _moves;
}

/**
 * \brief Metoda zwracająca wymiar planszy
 *
 * \return wymiar planszy
 *
 */
int GameController::GetDimension() const
{
    return _game->GetParameters().Dimension;
}

/**
 * \brief Metoda obsłgująca ustawienie pustej komórki we wskazanym polu
 *
 * \param position pozycja, która ma być pustym polem
 * \return informacja o powodzeniu operacji
 *
 */
bool GameController::MoveToBlank(Position position)
{
    _blankFieldBackwardPositions.push_back(_board.GetBlankPosition());
    if (_blankFieldBackwardPositions.size() > 3)
    {
        _blankFieldBackwardPositions.erase(_blankFieldBackwardPositions.begin());
    }

    if (_board.MoveToBlank(position))
    {
        _moves++;
        _blankFieldForwardPositions.clear();
        return true;
    }

    _blankFieldBackwardPositions.pop_back();
    return false;
}

/**
 * \brief Metoda sprawdzająca czy gra jest rozwiązana
 *
 * \return true, jeżeli gra jest skończona
 *
 */
bool GameController::IsFinished() const
{
    auto values = _board.GetValues();
    auto it = values.begin();

    for(auto val: values)
    {
        it++;

        if (it->second == 0)
            break;

        if (val.second >= it->second)
            return false;
    }

    return (it->first == Position(GetDimension() - 1, GetDimension() - 1));
}

/**
 * \brief Metoda obsługująca ruch do pozycji w przód
 *
 * \return informacja o powodzeniu operacji
 *
 */
bool GameController::NavigateForward()
{
    if (CanNavigateForward())
    {
        _blankFieldBackwardPositions.push_back(_board.GetBlankPosition());

        if (_board.MoveToBlank(_blankFieldForwardPositions.back()))
        {
            _blankFieldForwardPositions.pop_back();
            _moves++;

            return true;
        }
        _blankFieldBackwardPositions.pop_back();
    }

    return false;
}

/**
 * \brief Metoda obsługująca cofnięcie pozycji do wcześniejszej
 *
 * \return informacja o powodzeniu operacji
 *
 */
bool GameController::NavigateBackward()
{
    if (CanNavigateBackward())
    {
        _blankFieldForwardPositions.push_back(_board.GetBlankPosition());

        if (_board.MoveToBlank(_blankFieldBackwardPositions.back()))
        {

            _blankFieldBackwardPositions.pop_back();
            _moves--;

            return true;
        }
        _blankFieldForwardPositions.pop_back();
    }

    return false;
}

/**
 * \brief Metoda sprawdzająca czy możliwe jest cofnięcie pozycji do wcześniejszej
 *
 * \return czy jest możliwe cofnięcie
 *
 */
bool GameController::CanNavigateBackward() const
{
    return _blankFieldBackwardPositions.size() > 0;
}

/**
 * \brief Metoda sprawdzająca czy możliwe jest poruszenie pozycji wprzód
 *
 * \return czy jest możliwe poruszenie pozycji wprzód
 *
 */
bool GameController::CanNavigateForward() const
{
    return _blankFieldForwardPositions.size() > 0;
}

/**
 * \brief Metoda obsłgująca zapis stanu gry
 *
 * Zapisuje aktualny stan gry w celu jej kontynuowania w przyszłości
 *
 */
bool GameController::SaveGameCurrentState(int elapsedTime)
{
    _game->Update(GetValuesVector(), elapsedTime, _moves);
    GameLoaderSaver gameLoaderSaver;
    return gameLoaderSaver.Save(*_game);
}

/**
 * \brief Metoda obsłgująca zapis parametrów statycznych gry
 *
 * Zapisuje grę (jej parametry - stan początkowy, nazwa, wymiar) w celu zagrania konkretnego
 * ułożenia w przyszłości
 *
 */
bool GameController::SaveGame()
{
    GameParametersLoaderSaver saver;
    return saver.Save(_game->GetParameters());
}

/**
 * \brief Metoda zwracająca nazwę gracza
 *
 * \return nazwa gracza
 *
 */
std::string GameController::GetPlayerName() const
{
    return _game->GetPlayerName();
}

/**
 * \brief Metoda zwracająca nazwę gry
 *
 * \return nazwa gry
 *
 */
std::string GameController::GetGameName() const
{
    return _game->GetParameters().GameName;
}
