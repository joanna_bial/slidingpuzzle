#ifndef BOARD_H
#define BOARD_H

#include <map>
#include <set>
#include <vector>

#include "Position.h"

/**
 * \brief Szablon opisujący planszę
 *
 * Zawiera mapę współrzędnych i obiektów, wymiar i pozycję pustej komórki
 * Obsługuje operacje przesunięć na planszy
 *
 */
template<class T>
class Board
{
public:
    Board();
    Board(std::vector<T> values, Position blankPosition);
    void InsertValues(std::vector<T> values, Position blankPosition);
    bool MoveToBlank(Position position);
    std::map<Position, T> GetValues() const;
    Position GetBlankPosition() const;
    std::vector<Position> GetBlankNeighbors() const;
    Position* GetPositionForValue(T value) const;

private:
    typename std::map<Position, T> _boardMap;
    Position _blankPosition;
    int _dimension;

    bool CanBeMovedToBlank(Position position);
    bool IsPositionCorrect(Position position);
};

#endif // BOARD_H
