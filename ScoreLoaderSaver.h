#ifndef SCORELOADERSAVER_H
#define SCORELOADERSAVER_H

#include "LoaderSaver.h"
#include "Score.h"


//! Klasa obsugująca zapisanie i odczytanie wyniku gry
/*!
   Zapisuje i odczytuję wynik
*/
class ScoreLoaderSaver : LoaderSaver
{
public:
    ScoreLoaderSaver();
    std::vector<Score> Load();
    bool Save(Score score);
    std::string Serialize(Score score);
    std::vector<Score> Deserialize(std::vector<std::string> lines);

private:
    virtual bool ShouldClear() const override;
};

#endif // SCORELOADERSAVER_H
